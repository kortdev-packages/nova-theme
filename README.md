# Kort.dev Repsonsive Nova Styling
[![Latest Version on Packagist](https://img.shields.io/packagist/v/kortdev/nova-theme.svg?style=flat-square)](https://packagist.org/packages/kortdev/nova-theme)
[![Total Downloads](https://img.shields.io/packagist/dt/kortdev/nova-theme.svg?style=flat-square)](https://packagist.org/packages/kortdev/nova-theme)

### Installation

You can install the nova theme into a Laravel app that uses [Nova](https://nova.laravel.com) via composer:
```bash
composer require kortdev/nova-theme
```

This theme includes adapted Nova blade files based on Nova Responsive Theme and some config based options. To use them, publish the view, style & config files:

```bash
php artisan vendor:publish --provider="Kortdev\NovaTheme\ThemeServiceProvider" --force
```

And then you can configure the options editing the `config/nova-theme.php` file.

- - -

## Credits
Responsive design is based on Nova Responsive Theme by Gregoriohc.
See https://github.com/gregoriohc/laravel-nova-theme-responsive - [Gregorio Hernández Caso](https://github.com/gregoriohc)

Copyright (c) 2020 Kortdev.

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
