<p class="mt-8 text-center text-xs text-80">
    <a href="https://kort.dev" class="text-primary dim no-underline">DashKort</a>
    <span class="px-1">&middot;</span>
    &copy; {{ date('Y') }} - by kort.dev
    <span class="px-1">&middot;</span>
    Laravel Nova v{{ \Laravel\Nova\Nova::version() }}
</p>
