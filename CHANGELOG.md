# Changelog

All notable changes to `kortdev/nova-theme` will be documented in this file.

Updates should follow the [Keep a CHANGELOG](http://keepachangelog.com/) principles.

## 0.0.9 - 2020-09-14

- added Responsive support [Gregorio Hernández Caso](https://github.com/gregoriohc)
- added Laravel Nova (3.7) support
