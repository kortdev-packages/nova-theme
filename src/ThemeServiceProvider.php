<?php

namespace Kortdev\NovaTheme;

use Laravel\Nova\Nova;
use Laravel\Nova\Events\ServingNova;
use Illuminate\Support\ServiceProvider;

class ThemeServiceProvider extends ServiceProvider
{

    const NOVA_VIEWS_PATH =  __DIR__ . '/../resources/views';
    const CSS_PATH =  __DIR__.'/../resources/css';
    const JS_PATH =  __DIR__.'/../resources/js';
    const CONFIG_FILE = __DIR__ . '/../config/nova-theme.php';

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // JS for Responsive design
        Nova::serving(function (ServingNova $event) {
            Nova::style('nova-theme', __DIR__.'/../resources/css/responsive.css');
            Nova::script('nova-theme', __DIR__.'/../resources/js/responsive.js');
            Nova::provideToScript([
                'kdnt' => config('nova-theme')
            ]);
        });

        // Publishes Config
        $this->publishes([
            self::CONFIG_FILE => config_path('nova-theme.php'),
        ], 'config');

        // Views
        $this->publishes([
            self::NOVA_VIEWS_PATH => resource_path('views/vendor/nova')
        ]);

        // Publish Public CSS for login screen
        $this->publishes([
            self::CSS_PATH => public_path('vendor/kortdev/nova-theme'),
        ], 'public');

        // Sets CSS file as asset
        Nova::theme(asset('vendor/kortdev/nova-theme/theme.css'));
    }
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            self::CONFIG_FILE,
            'nova-theme'
        );
    }
}
